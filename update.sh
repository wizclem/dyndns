#!/bin/bash

source `dirname $0`"/includes.sh" || exit 99



echo_info "==== START ===="
update_dyndns_if_needed; update_exit_code=$?
echo_info "==== END ===="
exit $update_exit_code
