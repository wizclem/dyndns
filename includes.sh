################################################
# load configs
################################################
full_path=`realpath $0`
base_dir=`dirname $full_path`
config=$base_dir/config.sh
source $config || exit 21



################################################
# prerequisite functions
################################################
my_pid=$$
function echo_debug {
    $print_debug || return 0
    msg=$1
    echo "[$my_pid] [$(date -Isecond)] [debug] - $msg" 1>&2
}
function echo_info {
    msg=$1
    echo "[$my_pid] [$(date -Isecond)] [info] - $msg" 1>&2
}
function echo_warning {
    msg=$1
    echo "[$my_pid] [$(date -Isecond)] [WARNING] - $msg" 1>&2
}
function echo_error {
    msg=$1
    echo "[$my_pid] [$(date -Isecond)] [ERROR] - $msg" 1>&2
}
function elem_in_list {
    element=$1
    list=$2
    echo "$list" | grep --color "$element" > /dev/null; grep_exit_code=$?
    if [ $grep_exit_code -eq 0 ]
    then
        #echo_debug "element [$element] IS in list [$list]"
        return 0
    else
        #echo_debug "element [$element] is NOT in list [$list]"
        return 1
    fi
}
function dynstderr {
    $print_debug && echo "/dev/stderr" || echo "/dev/null"
}



################################################
# global variables
################################################
script_params=$@
#echo_debug "script_params[$script_params]"
#echo_debug "base_dir[$base_dir]"
logs_dir=$base_dir/logs
#echo_debug "logs_dir[$logs_dir]"
data_dir=$base_dir/data
#echo_debug "data_dir[$data_dir]"
script_lock=$data_dir/script.lock
timestamp=`date +%s`
my_id=$my_pid"_"$timestamp
#echo_debug "script_lock[$script_lock] my_id[$my_id]"



################################################
# dyndns functions
################################################
registered_ip_file=$data_dir/registered_ip.txt
ipv4_regex='[1-2]{0,1}[0-9]{0,1}[0-9]\.[1-2]{0,1}[0-9]{0,1}[0-9]\.[1-2]{0,1}[0-9]{0,1}[0-9]\.[1-2]{0,1}[0-9]{0,1}[0-9]'
function registered_ip {
    [ -f $registered_ip_file ] && cat $registered_ip_file || echo "no_registered_ip_yet"
}
function update_registered_ip_to {
    new_ip="$1"
    echo "$new_ip" > $registered_ip_file
}
function myipdotcom_ip {
    echo_debug "Trying to get IP via ==myip.com== ..."
    curl --ipv4 --max-time 10 https://api.myip.com 2>> $(dynstderr) | egrep --only-matching "$ipv4_regex" && return 0
    return 1
}
function whatismyipdotcom_ip {
    echo_debug "Trying to get IP via ==whatismyip.com== ..."
    curl --ipv4 --max-time 10 https://www.whatismyip.com/ 2>> $(dynstderr) | egrep --only-matching 'My Public IPv4 is.{100}' | egrep --only-matching "$ipv4_regex" && return 0
    return 1
}
function korben_ip {
    echo_debug "Trying to get IP via ==korben.info== ..."
    curl --ipv4 --max-time 10 https://services.korben.info/ip 2>> $(dynstderr) | egrep --only-matching 'Adresse IP publique.{100}' | egrep --only-matching "$ipv4_regex" && return 0
    return 1
}
function real_pub_ip {
    ip=$(myipdotcom_ip) && echo $ip && return 0
    ip=$(whatismyipdotcom_ip) && echo $ip && return 0
    ip=$(korben_ip) && echo $ip && return 0
    echo_error "Failed to get real public IP ><"
    echo "unknown" && return 1
}
function update_needed {
    registered_ip="$(registered_ip)"
    real_pub_ip="$(real_pub_ip)" || return 99
    echo "$real_pub_ip"
    if [ "$real_pub_ip" != "$registered_ip" ]
    then
        echo_info "UPDATE NEEDED (real_pub_ip[$real_pub_ip] != registered_ip[$registered_ip])"
        return 0
    else
        echo_debug "Update not needed (real_pub_ip[$real_pub_ip] == registered_ip[$registered_ip])"
        return 1
    fi
}
function update_dyndns {
    new_ip="$1"
    url=$(echo "$dyn_provider_full_url" | sed "s,<dyn_ip_placeholder_dont_touch_me>,$new_ip,")
    echo_info "Updating [$dyn_sub_domain] with IP [$new_ip] (via [$url])..."
    #curl --ipv4 --max-time 15 --basic --user "$dyn_user:$dyn_password" $url; exit 99
    curl --ipv4 --max-time 15 --basic --user "$dyn_user:$dyn_password" $url 2>> $(dynstderr) | egrep "(good|nochg)"; grep_exit_code=$?
    if [ $grep_exit_code -eq 0 ]
    then
        echo_info "ok, dyndns update sucessfull :D"
        update_registered_ip_to "$new_ip"
        return 0
    else
        echo_error "dyndns update FAILED ><"
        return 1
    fi
}
function update_dyndns_if_needed {
    new_ip="$(update_needed)"; update_needed_exit_code=$?
    [ $update_needed_exit_code -eq 1 ] && return 0 # alredy up to date
    [ $update_needed_exit_code -ne 0 ] && return $update_needed_exit_code # something went wrong
    update_dyndns "$new_ip" || return 1
}
