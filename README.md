# dyndns

## Description
A simple IPv4 bash script for dyndns updates.

## Prerequisites
- A bash interpreter (for example, any modern linux distribution)
- The following executable(s) : `curl`

## Installation
1. Clone/download this repo somewhere (avoid directory names with spaces).
1. (optional but recommended) Audit the source code.
1. Copy "config.template.sh" into "config.sh" and customise its content.
1. (optional but recommended) Login to your dyndns provider and force an inexact IP, like `1.1.1.1`.
1. Execute "update.sh" manually,
    1. Login to your dyndns provider and check if the IP has been updated.
    1. If not ok :
        1. Switch "print_debug" from `false` to `true` into your "config.sh",
        1. Execute "update.sh" again.
    1. To force update from scratch, remove the file "data/registered_ip.txt".
1. Put the script into your crontab, for example : `* *    * * *   sleep an_integer_between_5_and_55; cd /where/you/cloned/the/repo/dyndns && ./update.sh >> logs/update.log 2>&1`
    1. replace "an_integer_between_5_and_55" with an interger between 5 and 55. For example : `sleep 42;`
1. Wait two minutes and check the content of "logs/update.log".

## Usage
```
./update.sh
    [13945] [2021-09-05T20:38:37+02:00] [info] - ==== START ====
    [13945] [2021-09-05T20:38:37+02:00] [info] - UPDATE NEEDED (real_pub_ip[A.B.C.D] != registered_ip[E.F.G.H])
    [13945] [2021-09-05T20:38:37+02:00] [info] - Updating [dyndns-subdomain.your.domain] with IP [A.B.C.D] (via [https://your.dyn-dns.provider/nic/update?system=dyndns&hostname=dyndns-subdomain.your.domain&myip=A.B.C.D])...
    good A.B.C.D
    [13945] [2021-09-05T20:38:39+02:00] [info] - ok, dyndns update sucessfull :D
    [13945] [2021-09-05T20:38:39+02:00] [info] - ==== END ====

./update.sh
    [13978] [2021-09-05T20:38:53+02:00] [info] - ==== START ====
    [13978] [2021-09-05T20:38:53+02:00] [info] - ==== END ====

rm data/registered_ip.txt
./update.sh
    [14091] [2021-09-05T20:44:23+02:00] [info] - ==== START ====
    [14091] [2021-09-05T20:44:24+02:00] [info] - UPDATE NEEDED (real_pub_ip[A.B.C.D] != registered_ip[no_registered_ip_yet])
    [14091] [2021-09-05T20:44:24+02:00] [info] - Updating [dyndns-subdomain.your.domain] with IP [A.B.C.D] (via [https://your.dyn-dns.provider/nic/update?system=dyndns&hostname=dyndns-subdomain.your.domain&myip=A.B.C.D])...
    nochg A.B.C.D
    [14091] [2021-09-05T20:44:25+02:00] [info] - ok, dyndns update sucessfull :D
    [14091] [2021-09-05T20:44:25+02:00] [info] - ==== END ====
```

## Roadmap
No active roadmap.

## Contributing
Open an issue to discuss it.

## Authors and acknowledgment
External IP detected thanks to these services :
- myip.com (main)
- whatismyip.com (failover)
- korben.info (failover of the failover)

## License
See "LICENSE" file.
