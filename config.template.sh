# copy and update me in a new file called "config.sh"
dyn_user='your dyndns user'
dyn_password='your dyndns password'
dyn_sub_domain='dyndns-subdomain.your.domain'
dyn_provider_full_url="https://your.dyn-dns.provider/nic/update?system=dyndns&hostname=$dyn_sub_domain&myip=<dyn_ip_placeholder_dont_touch_me>"
print_debug=false


# known providers (by alphabetical order) :
#
# dyn.com : "https://members.dyndns.org/v3/update?hostname=$dyn_sub_domain&myip=<dyn_ip_placeholder_dont_touch_me>" (? untested)
# dynv6.com : "https://dynv6.com/nic/update?hostname=$dyn_sub_domain&myip=<dyn_ip_placeholder_dont_touch_me>" (? untested)
# noip.com : "https://dynupdate.no-ip.com/nic/update?hostname=$dyn_sub_domain&myip=<dyn_ip_placeholder_dont_touch_me>" (? untested)
# ovh.com : "https://www.ovh.com/nic/update?system=dyndns&hostname=$dyn_sub_domain&myip=<dyn_ip_placeholder_dont_touch_me>"
